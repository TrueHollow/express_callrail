const log4js = require('log4js');
const bodyParser = require('body-parser');
const cors = require('cors');
const session = require('express-session');
const passport = require('passport');
const config = require('../config');

/**
 * Set common middleware for Express application
 * @param app Express application
 * @param logger Logger object (hack for using with express)
 */
module.exports = (app, logger) => {
  app.use(
    log4js.connectLogger(logger, {
      level: 'auto',
      format: (req, res, format) =>
        format(
          ':remote-addr ":method :url HTTP/:http-version" :status :content-length ":referrer" ":user-agent"'
        ),
    })
  );
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(cors());
  app.use(
    session({
      secret: 'express_callrail_SESSION',
      resave: false,
      saveUninitialized: false,
      cookie: { httpOnly: true },
    })
  );
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => {
    done(null, user.username);
  });

  passport.deserializeUser((username, done) => {
    const user = config.credentials.find(credential => {
      return credential.username === username;
    });
    done(null, user);
  });
};
