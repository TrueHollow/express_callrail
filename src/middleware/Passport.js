const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const config = require('../config');

module.exports = () => {
  passport.use(
    new LocalStrategy((username, password, done) => {
      const user = config.credentials.find(credential => {
        return (
          credential.username === username && credential.password === password
        );
      });
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    })
  );
};
