const env = require('env-var');
const CallRailProvider = require('./CallRailProvider');
const IO = require('./IO');

const CallRailAccoundId = env
  .get('CallRailAccoundId')
  .required()
  .asString();

const CallRailToken = env
  .get('CallRailToken')
  .required()
  .asString();

class Backend {
  constructor() {
    this.callRailProvider = new CallRailProvider(
      CallRailAccoundId,
      CallRailToken
    );
    this.io = new IO();
  }

  /**
   * Get Call Rail Provider
   * @return {CallRailProvider}
   */
  get CallRailProvider() {
    return this.callRailProvider;
  }

  /**
   * Get IO
   * @return {IO}
   */
  get IO() {
    return this.io;
  }
}

module.exports = Backend;
