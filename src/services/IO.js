const AdmZip = require('adm-zip');
const stringify = require('csv-stringify');
const XLSX = require('xlsx');
const Logger = require('../common/Logger');

class IO {
  constructor() {
    this.logger = Logger('src/services/IO.js');
  }

  /**
   * Convert array of objects to csv string
   * @param {Array<Object>} arr
   * @return {Promise<string>}
   */
  async getCSVString(arr) {
    this.logger.debug('Converting array into csv string');
    return new Promise((resolve, reject) => {
      const options = {
        delimiter: ',',
        header: true,
      };
      stringify(arr, options, (err, records) => {
        if (err) {
          return reject(err);
        }
        return resolve(records);
      });
    });
  }

  /**
   * Convert string to buffer
   * @param {string} str
   * @return {Buffer}
   */
  getBufferFromString(str) {
    this.logger.debug('Converting string into buffer (utf8)');
    return Buffer.from(str, 'utf8');
  }

  /**
   * Compress file to zip buffer
   * @param {string} fileName
   * @param {Buffer} content
   * @return {Buffer}
   */
  getZipBuffer(fileName, content) {
    this.logger.debug(`Compress ${fileName} into zip buffer`);
    const zip = new AdmZip();
    zip.addFile(fileName, content);
    return zip.toBuffer();
  }

  /**
   * Convert array of objects to xlsx buffer
   * @param {Array<Object>} arr
   */
  getXLSXBuffer(arr) {
    this.logger.debug(
      `Convert array of objects (length=${arr.length}) to xlsx`
    );
    const wb = XLSX.utils.book_new();
    const wsName = 'SheetJS';

    const ws = XLSX.utils.json_to_sheet(arr);

    /* Add the worksheet to the workbook */
    XLSX.utils.book_append_sheet(wb, ws, wsName);

    const wopts = { bookType: 'xlsx', bookSST: false, type: 'buffer' };

    return XLSX.write(wb, wopts);
  }
}

module.exports = IO;
