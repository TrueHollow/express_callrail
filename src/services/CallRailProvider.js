const moment = require('moment');
const rp = require('request-promise-native');

const loggerGenerator = require('../common/Logger');

const ADDITIONAL_USER_REQUESTED_FIELDS = [
  'company_id',
  'company_name',
  'company_time_zone',
  'created_at',
  'device_type',
  'first_call',
  'formatted_customer_location',
  'formatted_business_phone_number',
  'formatted_customer_name',
  'prior_calls',
  'formatted_customer_name_or_phone_number',
  'formatted_customer_phone_number',
  'formatted_duration',
  'formatted_tracking_phone_number',
  'formatted_tracking_source',
  'formatted_value',
  'good_lead_call_id',
  'good_lead_call_time',
  'lead_status',
  'note',
  'source',
  'source_name',
  'tags',
  'total_calls',
  'value',
  'waveforms',
  'tracker_id',
  'speaker_percent',
  'keywords',
  'medium',
  'referring_url',
  'landing_page_url',
  'last_requested_url',
  'referrer_domain',
  'utm_source',
  'utm_medium',
  'utm_term',
  'utm_content',
  'utm_campaign',
  'utma',
  'utmb',
  'utmc',
  'utmv',
  'utmz',
  'ga',
  'gclid',
  'fbclid',
  'keywords_spotted',
  'call_highlights',
  'agent_email',
  'campaign',
];

const LISTING_CALLS_DEFAULT_FIELDS = ADDITIONAL_USER_REQUESTED_FIELDS.join(',');

class CallRailProvider {
  /**
   * Create provider
   * @param {string|Number} accountId
   * @param {string} token
   */
  constructor(accountId, token) {
    this.logger = loggerGenerator(
      `src/services/CallRailProvider.js-${accountId}`
    );
    this.accountId = accountId;
    this.token = token;
  }

  /**
   * Get summary
   * @param {Object} params
   * @param {string|Number} params.companyId
   * @param {string} params.fields
   * @param {string} params.groupBy
   * @param {string} params.endDate
   * @param {string} params.startDate
   * @return {Promise<Object>}
   * @constructor
   */
  async GetSummary({ companyId, fields, groupBy, endDate, startDate }) {
    const cleanStartDate = moment(startDate).format('YYYY-MM-DD');
    const cleanEndDate = moment(endDate).format('YYYY-MM-DD');
    this.logger.debug(
      `GetSummary: ${companyId}, ${fields}, ${groupBy}, ${cleanStartDate}, ${cleanEndDate}`
    );
    return rp({
      url: `https://api.callrail.com/v3/a/${this.accountId}/calls/summary.json`,
      qs: {
        company_id: companyId,
        fields,
        group_by: groupBy,
        end_date: cleanEndDate,
        start_date: cleanStartDate,
      },
      headers: {
        Authorization: `Token token=${this.token}`,
      },
      json: true,
    });
  }

  /**
   * Get listing all calls
   * https://apidocs.callrail.com/#listing-all-calls
   * @param {Object} params
   * @param {string|Number} params.companyId
   * @param {string} params.endDate
   * @param {string} params.startDate
   * @param {string} params.fields
   * @return {Promise<Array<Object>>}
   */
  async GetListingAllCalls({ companyId, endDate, startDate, fields }) {
    const cleanStartDate = moment(startDate).format('YYYY-MM-DD');
    const cleanEndDate = moment(endDate).format('YYYY-MM-DD');
    const cleanFields = !fields ? LISTING_CALLS_DEFAULT_FIELDS : fields;
    this.logger.debug(
      `GetListingAllCalls: ${companyId}, ${cleanStartDate}, ${cleanEndDate}, ${cleanFields}`
    );
    const totalResult = [];
    let shouldContinue = false;
    let page = 1;
    do {
      // eslint-disable-next-line no-await-in-loop
      const iterationResult = await rp({
        url: `https://api.callrail.com/v3/a/${this.accountId}/calls.json`,
        qs: {
          company_id: companyId,
          end_date: cleanEndDate,
          start_date: cleanStartDate,
          page,
          fields: cleanFields,
        },
        headers: {
          Authorization: `Token token=${this.token}`,
        },
        json: true,
      });
      page += 1;
      shouldContinue = iterationResult.page < iterationResult.total_pages;
      totalResult.push(...iterationResult.calls);
    } while (shouldContinue);
    return totalResult;
  }
}

module.exports = CallRailProvider;
