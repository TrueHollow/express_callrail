require('./common/dotEnv_proxy');
const path = require('path');
const express = require('express');
const config = require('./config');
const middleware = require('./middleware');
const Passport = require('./middleware/Passport');
const Backend = require('./services/Backend');
const routes = require('./routes');
const logger = require('./common/Logger')('src/index.js');

/**
 * Express application
 * @type {app}
 */
const app = express();

app.set('trust proxy', 1); // trust first proxy (used for Heroku)
app.set('view engine', 'ejs');
app.set('views', path.resolve(__dirname, 'views'));

const backend = new Backend();
middleware(app, logger);
Passport();
routes(app, backend);

const staticFilesDirectory = path.resolve(__dirname, 'static');
app.use(express.static(staticFilesDirectory));

app.use('*', (req, res) => {
  res.status(404);
  res.json({
    message: 'Not found',
  });
});

app.use((error, req, res /* next */) => {
  res.status(error.status || 500);
  logger.error(error);
  res.json({
    status: error.status,
    message: error.message,
    // stack: error.stack,
  });
});

const httpPort = process.env.PORT || config.express.http_port;

const listener = app.listen(httpPort);
logger.info(`app running on port http://localhost:${httpPort} ...`);

module.exports = { app, listener };
