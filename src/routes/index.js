const passport = require('passport');
const HttpStatus = require('http-status-codes');
const asyncHandler = require('express-async-handler');

function mustAuthenticated(req, res, next) {
  if (!req.isAuthenticated()) {
    res.status(HttpStatus.UNAUTHORIZED).send({});
  } else {
    next();
  }
}

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    res.redirect('/service');
  } else {
    next();
  }
}

module.exports = (app, backend) => {
  app.get('/', isAuthenticated, (req, res) => {
    res.render('index');
  });
  app.get('/service', mustAuthenticated, (req, res) => {
    res.render('service');
  });
  app.post(
    '/login',
    passport.authenticate('local', {
      successRedirect: '/service',
      failureRedirect: '/',
    })
  );
  app.get('/logout', mustAuthenticated, (req, res) => {
    req.logout();
    req.session.destroy();
    res.redirect('/');
  });
  app.post(
    '/api/v1/listingAllCalls',
    mustAuthenticated,
    asyncHandler(async (req, res) => {
      const { body: data } = req;
      const provider = backend.CallRailProvider;
      const result = await provider.GetListingAllCalls(data);
      const io = backend.IO;
      const attachmentFormat =
        typeof data.attachmentFormat === 'string'
          ? data.attachmentFormat.toLowerCase()
          : undefined;
      let fileName;
      let sendBuffer;
      switch (attachmentFormat) {
        default:
        case 'csv':
          // eslint-disable-next-line no-case-declarations
          const csvString = await io.getCSVString(result);
          // eslint-disable-next-line no-case-declarations
          const buffer = io.getBufferFromString(csvString);
          // eslint-disable-next-line no-case-declarations
          const csvFileName = 'listingAllCalls.csv';
          fileName = `${csvFileName}.zip`;
          sendBuffer = await io.getZipBuffer(csvFileName, buffer);
          break;
        case 'xlsx':
          fileName = 'listingAllCalls.xlsx';
          sendBuffer = io.getXLSXBuffer(result);
      }
      res.attachment(fileName);
      res.send(sendBuffer);
    })
  );
};
