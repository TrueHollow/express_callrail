module.exports = {
  /**
   * Logger common configuration
   */
  log4js: {
    appenders: {
      out: {
        type: 'stdout',
      },
    },
    categories: {
      default: {
        appenders: ['out'],
        level: 'debug',
      },
    },
  },
  /**
   * Default express http port
   */
  express: {
    http_port: 3000,
  },
  credentials: [
    {
      username: '1',
      password: '1',
    },
    {
      username: 'test',
      password: 'test123',
    },
  ],
};
