const moment = require('moment');
const chai = require('chai');
chai.use(require('chai-http'));
const config = require('../src/config');

const { expect } = chai;
const { app, listener } = require('../src/index');

describe('App test', () => {
  before(async () => {});
  after(async () => {
    listener.close();
  });
  it('test POST method for /login', async () => {
    const agent = chai.request.agent(app);
    const [credentials] = config.credentials;
    const res = await agent.post('/login').send(credentials);
    expect(res).to.have.status(200);
    expect(res).to.redirectTo(/\/service$/);
    // eslint-disable-next-line no-unused-expressions
    expect(res).to.be.html;
    agent.close();
  });
  it('test POST method for /api/v1/listingAllCalls (200)', async () => {
    const agent = chai.request.agent(app);
    const [credentials] = config.credentials;
    await agent.post('/login').send(credentials);
    const now = moment();
    const prevMonth = now.subtract(1, 'month');
    const startDate = prevMonth.startOf('month').format('YYYY-MM-DD');
    const endDate = prevMonth.endOf('month').format('YYYY-MM-DD');
    const parameters = {
      companyId: 361072581,
      endDate,
      startDate,
    };
    const res = await agent.post('/api/v1/listingAllCalls').send(parameters);
    expect(res).to.have.status(200);
    expect(res).to.have.header('Content-Disposition');
    expect(res).to.have.header('Content-Type', 'application/zip');
    agent.close();
  });
});
